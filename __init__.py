from st3m.application import Application, ApplicationContext
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK, WHITE
from st3m.goose import Dict, Any
from st3m.input import InputController, InputState
from ctx import Context
import bl00mbox
import leds
import audio
import json
import math
import sys
#import audio_mp3

class Configuration:
    def __init__(self) -> None:
        self.name = "flow3r"
        self.team = "visitor"
        self.size: int = 75
        self.font: int = 5
        self.pronouns: list[str] = []
        self.pronouns_size: int = 25
        self.color = "0x40ff22"
        self.mode = 0

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "name" in data and type(data["name"]) == str:
            res.name = data["name"]
        if "team" in data and type(data["team"]) == str:
            res.team = data["team"]
        if "size" in data:
            if type(data["size"]) == float:
                res.size = int(data["size"])
            if type(data["size"]) == int:
                res.size = data["size"]
        if "font" in data and type(data["font"]) == int:
            res.font = data["font"]
        # type checks don't look inside collections
        if (
            "pronouns" in data
            and type(data["pronouns"]) == list
            and set([type(x) for x in data["pronouns"]]) == {str}
        ):
            res.pronouns = data["pronouns"]
        if "pronouns_size" in data:
            if type(data["pronouns_size"]) == float:
                res.pronouns_size = int(data["pronouns_size"])
            if type(data["pronouns_size"]) == int:
                res.pronouns_size = data["pronouns_size"]
        if (
            "color" in data
            and type(data["color"]) == str
            and data["color"][0:2] == "0x"
            and len(data["color"]) == 8
        ):
            res.color = data["color"]
        if "mode" in data:
            if type(data["mode"]) == float:
                res.mode = int(data["mode"])
            if type(data["mode"]) == int:
                res.mode = data["mode"]
        return res

    def save(self, path: str) -> None:
        d = {
            "name": self.name,
            "size": self.size,
            "font": self.font,
            "pronouns": self.pronouns,
            "pronouns_size": self.pronouns_size,
            "color": self.color,
            "mode": self.mode,
        }
        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()

    def to_normalized_tuple(self) -> Tuple[float, float, float]:
        return (
            int(self.color[2:4], 16) / 255.0,
            int(self.color[4:6], 16) / 255.0,
            int(self.color[6:8], 16) / 255.0,
        )


class SosiApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._state = 0
        self._time = 0
        self._soundtime = 0
        self._wait_time = 100

        self._light = False
        self._sound = False

        self._filename = "/flash/nick.json"
        self._config = Configuration.load(self._filename)
        self._input = InputController()
        
        self._scale_name = 1.0
        self._scale_pronouns = 1.0
        self._filename = "/flash/nick.json"
        self._config = Configuration.load(self._filename)
        self._pronouns_serialized = " ".join(self._config.pronouns)
        
        bundle_path = app_ctx.bundle_path
        if bundle_path == "":
            bundle_path = "/flash/sys/apps/higtower-flow3r-sosi"

        
        self._ch = bl00mbox.Channel("SoSi")
        sys.stdout.write("load: " + bundle_path + "/beedo.wav")
        self._beedo = self._ch.new(bl00mbox.patches.sampler, bundle_path + "/beedo.wav")
        self._ch.volume = 9000

        audio.speaker_set_volume_dB(audio.speaker_get_maximum_volume_dB())
        
        #self._ts = self._ch.new(bl00mbox.patches.tinysynth)
        
        #self._mp3 = st3m_media_load_mp3("beedo.mp3")
        


    def draw_blue(self, ctx: Context) -> None: 
        ctx.rgb(0, 0, 255).rectangle(-120, -120, 240, 240).fill()
        leds.set_all_rgb(0, 0, 255)
        leds.update()
        
    def draw_black(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        leds.set_all_rgb(0, 0, 0)
        leds.update()

    def draw(self, ctx: Context) -> None:
        
        if self._state % 2 == 1 :
            self.draw_black(ctx)
        else:
            self.draw_blue(ctx)
        
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = self._config.size
        ctx.font = ctx.get_font_name(self._config.font)
        
        ctx.rgb(*WHITE)

        ctx.move_to(0, 0)
        ctx.save()
        ctx.scale(self._scale_name, 1)
        ctx.text(self._config.name)
        ctx.restore()

        if self._pronouns_serialized:
            ctx.move_to(0, +60)
            ctx.font_size = self._config.pronouns_size
            ctx.save()
            ctx.scale(self._scale_pronouns, 1)
            ctx.text(self._config.team)
            ctx.restore()
            
        ctx.move_to(0, -60)
        ctx.font_size = self._config.pronouns_size
        ctx.save()
        ctx.scale(self._scale_pronouns, 1)
        ctx.text(self._pronouns_serialized)
        ctx.restore()


        leds.update()
        # ctx.fill()

    def on_exit(self) -> None:
        self._light = False
        self._sound = False
        leds.set_all_rgb(0, 0, 0)
        leds.update()
        self._beedo.signals.trigger.stop()

    def think(self, ins: InputState, delta_ms: int) -> None:
        self._input.think(ins, delta_ms)

        
        if self._input.buttons.app.left.pressed:
            self._light = not self._light
            if not self._light :
                self._sound = False
        elif self._light and self._input.buttons.app.right.pressed:
            self._sound = not self._sound
        
        if self._light :
            self._time += delta_ms
            if self._time > self._wait_time :
                self._time = 0
                self._state += 1
                self._wait_time = 100
            
            
                if self._state == 5 :
                    self._wait_time = 500
                if self._state > 5 :
                    self._state = 0
        else:
            self._state = 1
            self._time = 0
        
        if self._sound :
            self._soundtime += delta_ms
            if self._soundtime > 890 :
                self._soundtime = 0
                self._beedo.signals.output = self._ch.mixer
                self._beedo.signals.trigger.start()
                    
            #mp3_think(self._mp3, delta_ms)
            #self._ts.signals.pitch.tone = self._soundstate * 2
            
            
        else:
            self._beedo.signals.trigger.stop()

        super().think(ins, delta_ms)
        